$(document).ready(function(){
$('.items').slick({
dots: true,
infinite: true,
speed: 800,
autoplay: false,
autoplaySpeed: 2000,
slidesToShow: 4,
interval: false,
slidesToScroll: 4,
responsive: [
{
breakpoint: 1024,
settings: {
slidesToShow: 3,
slidesToScroll: 3,
infinite: true,
dots: true
}
},
{
breakpoint: 600,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
},
{
breakpoint: 480,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
},
{
breakpoint: 320,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
}

]
});
$('.items1').slick({
dots: true,
infinite: true,
speed: 800,
autoplay: false,
autoplaySpeed: 2000,
slidesToShow: 3,
interval: false,
slidesToScroll: 3,
responsive: [
{
breakpoint: 1024,
settings: {
slidesToShow: 3,
slidesToScroll: 3,
infinite: true,
dots: true
}
},
{
breakpoint: 600,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
},
{
breakpoint: 480,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
},
{
breakpoint: 320,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
}

]
});
});